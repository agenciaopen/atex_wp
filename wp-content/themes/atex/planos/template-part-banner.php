<?php
global $post;
$page_ID = $post->ID;
// get page ID

if (wp_is_mobile()) :
    $featured_img_url = get_the_post_thumbnail_url(get_the_ID($page_ID), 'full');
else :
    $featured_img_url = get_the_post_thumbnail_url(get_the_ID($page_ID), 'large');
endif;
?>
<?php if (get_field('titulop', $page_ID)) : ?>
    <?php $title = get_field('titulop', $page_ID); ?>
<?php else : ?>
    <?php $title = get_the_title(); ?>
<?php endif; ?>

<section  style="background-image: url('<?php echo $featured_img_url; ?>');">
    <div class="container h-100">
        <div class="row h-100 align-items-end justify-content-center">
            <div class="col-md-12 text-center">
                <h1>
                    <?php echo $title; ?>
                    <span class="d-block"><?php the_field('subtitulop', $page_ID); ?></span>
                </h1>
                <hr>
                <h2><?php the_field('descricaop', $page_ID, false, false); ?></h2>
            </div>
        </div>
    </div>
</section><!-- /.main -->