<section class="newsletter pb-0">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-md-10 justify-content-between card">
                <div class="col-md-5 card-header">
                    <h3><?php the_field( 'titulo_newsletter', 'option' ); ?></h3>
                    <p><?php the_field( 'descricao_newsletter', 'option' ); ?></p>
                </div>    
                <div class="col-md-6 card-content">
                    <?php the_field( 'formulario_newsletter', 'option' ); ?>
                </div>
            </div>

            <!--<div class="col-md-12 text-center">
                <h3><?php// the_field( 'titulo_newsletter', 'option' ); ?></h3>
            </div>
            <div class="col-md-8 col-lg-7 text-center">
                <p><?php //the_field( 'descricao_newsletter', 'option' ); ?></p>
            </div>
            <div class="col-md-12 text-center">
                <?php// the_field( 'formulario_newsletter', 'option' ); ?>
            </div>-->
        </div>
    </div>
</section><!-- /.newsletter -->


