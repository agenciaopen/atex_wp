<?php

/**
 *
 * Template Name: Blog
 *
 */

get_header();
global $post;
$page_ID = $post->ID;
// get page ID 
?>


<?php get_template_part('global/template-part', 'banner'); ?>

<section class="search_article">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="category_article hide-mobile" id="category_article">

                    <ul>
                        <li><a href="/blog">Todos os artigos</a></li>
                        <?php wp_list_categories(array(
                            'orderby' => 'name',
                            'title_li' => '',
                        )); ?>
                    </ul>
                </div>
                <div class="select hide-desk">
                    <?php wp_dropdown_categories('show_option_none=Selecione uma categoria'); ?>

                    <div class="select_arrow">
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="header_article ">
                    <h2>Todos os artigos</h2>
                    <p>Confira diversos artigos que podem ser alicerces para seus projetos e carreira</p>
                </div>
                <div class="content_result_article">
                    <?php echo do_shortcode('[ajax_load_more container_type="div" scroll="false" post_type="post" posts_per_page="6" button_label="Mostrar mais" button_loading_label="Carregando..." button_done_label="Concluído"]'); ?>

                    
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_template_part('global/template-part', 'newsletter'); ?>

<?php get_template_part('global/template-part', 'aqui-tem-atex'); ?>

<?php get_footer(); ?>