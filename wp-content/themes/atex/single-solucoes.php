<?php
/**
*
* single page for cpt obras
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>
<?php while ( have_posts() ) : the_post(); ?>

<?php get_template_part( 'global/template-part', 'banner-produtos' ); ?>

<?php if ( have_rows( 'cadastro_da_pagina' ) ): ?>
	<?php while ( have_rows( 'cadastro_da_pagina' ) ) : the_row(); ?>
		<?php if ( get_row_layout() == 'construa_ou_conheca' ) : ?>
            <section class="main-sistema">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 box-left">
                            <h2><?php the_sub_field( 'construa_title' ); ?></h2>
                                <?php the_sub_field( 'construa_texto' ); ?>
                        </div>
                        <div class="col-md-6">
                            <div class="lajes_carousel">
                            <?php if ( have_rows( 'cadastro_de_itens_do_construa' ) ) : ?>
                                    <?php while ( have_rows( 'cadastro_de_itens_do_construa' ) ) : the_row(); ?>
                                        <div class="card">
                                            <div class="card-header">
                                                <?php if ( get_sub_field( 'icone' ) ) : ?>
                                                    <img src="<?php the_sub_field( 'icone' ); ?>" />
                                                <?php endif ?>
                                                <p><?php the_sub_field( 'titulo' ); ?></p>
                                            </div>
                                            <div class="card-content">
                                                <p><?php the_sub_field( 'subtitulo' ); ?></p>
                                            </div>
                                        </div>
                                    <?php endwhile; ?>
                                <?php else : ?>
                                    <?php // no rows found ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section><!--/.main-sistema-->
		<?php elseif ( get_row_layout() == 'produtos_relacionados' ) : ?>
        
<section class="produtos-solucoes">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h3><?php the_sub_field( 'produtos_relacionados_titulo' ); ?></h3>
            </div>
            <div class="col-md-11">
                <div class="p_solucoes_carousel">
                    <?php $produtos_relacionados_relacao = get_sub_field( 'produtos_relacionados_relacao' ); ?>
                        <?php if ( $produtos_relacionados_relacao ) : ?>
                            <?php foreach ( $produtos_relacionados_relacao as $post ) : ?>
                                <?php setup_postdata ( $post ); ?>
                                <div class="card">
                                    <div class="card-header col-md-5">
                                        <a href="<?php the_permalink(); ?>">
                                            <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
                                            echo the_post_thumbnail('full');?>
                                        </a>
                                    </div>
                                    <div class="card-content">
                                        <h3><?php the_title(); ?></h3>
                                        <p>
                                            <?php echo wp_strip_all_tags( get_the_excerpt(), true ); ?>
                                        </p>
                                        <div class="d-flex justify-content-end">
                                            <a href="<?php the_permalink(); ?>" id="<?php the_title(); ?>">
                                                <button class="btn btn_first mt-4">Detalhes do produto</button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                               
                            <?php endforeach; ?>
                            <?php wp_reset_postdata(); ?>
                        <?php endif; ?>                   
                </div>
                <div class="d-flex justify-content-center">
                    <a href="/produtos">
                        <button class="btn btn_first mt-4">Todos os produtos</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section><!--/.peodutos-solucoes-->
			
           
		<?php elseif ( get_row_layout() == 'modelos' ) : ?>

            <section class="modelos">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <h2><?php the_sub_field( 'titulo_modelos' ); ?></h2>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="modelos_carousel">
                                

                                    <?php if ( have_rows( 'cadastro_de_modelos' ) ) : ?>
                                        <?php while ( have_rows( 'cadastro_de_modelos' ) ) : the_row(); ?>
                                        <div class="col-md-3 p-2">
                                            <div class="card">
                                                <div class="card-header">
                                                    <?php $imagem = get_sub_field( 'imagem' ); ?>
                                                    <?php if ( $imagem ) : ?>
                                                        <img class="img-fluid" src="<?php echo esc_url( $imagem['url'] ); ?>" alt="<?php echo esc_attr( $imagem['alt'] ); ?>" />
                                                    <?php endif; ?>
                                                </div>
                                                <div class="card-content">
                                                    <h3><?php the_sub_field( 'titulo' ); ?></h3>
                                                    <p><?php the_sub_field( 'descricao' ); ?></p>
                                                </div>
                                            </div>
                                        </div>
                                            <?php endwhile; ?>
                                    <?php else : ?>
                                        <?php // no rows found ?>
                                    <?php endif; ?>


                                        
                                   
                            </div>
                        </div>
                    </div>
                </div>
            </section>



		<?php elseif ( get_row_layout() == 'como_funciona' ) : ?>
            <section class="como-funciona">
                <div class="container">
                    <div class="row">
                        <div class="col-md-7">
                            <h3><?php the_sub_field( 'como_funciona_titulo' ); ?></h3>
                            <p><?php the_sub_field( 'como_funciona_texto' ); ?></p>
                        </div>
                        <div class="content-video col-md-5">
                            <?php if ( get_sub_field( 'ativar_video' ) == 1 ) : ?>
                                <iframe id="construct_movie" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"width="100%" height="auto" type="text/html" src=""></iframe>
                                <input id="construct_movieID" type="hidden" value="<?php echo the_sub_field( 'video_como_funciona' ); ?>">

                                <script>
                                    var video_caller = function() {
                                    var get_vdId = document.getElementById('construct_movieID').value;
                                    var str_t = get_vdId.replace('https://www.youtube.com/watch?v=', '');
                                    var set_victim = document.getElementById('ytplayer');

                                    if (get_vdId.includes('https://youtu.be/') == true) {
                                        var str_t = get_vdId.replace('https://youtu.be/', '');

                                        document.getElementById('construct_movie').setAttribute('src', 'https://www.youtube.com/embed/' +
                                            str_t + '?rel=0?enablejsapi=1&autoplay=0');

                                    } else {
                                        var str_t = get_vdId.replace('https://www.youtube.com/watch?v=', '');
                                        document.getElementById('construct_movie').setAttribute('src', 'https://www.youtube.com/embed/' +
                                            str_t + '?rel=0?enablejsapi=1&autoplay=0');
                                        }
                                    }

                                    try {
                                        video_caller();
                                    } catch (error) {
                                        console.log(error);
                                    }
                                </script>
                            <?php else : ?>
                                <?php $imagem_como_funciona = get_sub_field( 'imagem_como_funciona' ); ?>
                                <?php if ( $imagem_como_funciona ) : ?>
                                    <img src="<?php echo esc_url( $imagem_como_funciona['url'] ); ?>" alt="<?php echo esc_attr( $imagem_como_funciona['alt'] ); ?>" />
                                <?php endif; ?>
                            <?php endif; ?>   
                        </div>
                        <div class="col-md-12"> 
                            <?php $baixarbotao = get_sub_field( 'baixarbotao' ); ?>
                                <?php if ( $baixarbotao ) : ?>
                                    <button class="btn btn_first mt-4 mb-4"><a class="text-uppercase text-white" href="<?php echo esc_url( $baixarbotao['url'] ); ?>" target="<?php echo esc_attr( $baixarbotao['target'] ); ?>"><?php echo esc_html( $baixarbotao['title'] ); ?></a></button>
                                <?php endif; ?><br>
                                <?php $segundo_botao = get_sub_field( 'segundo_botao' ); ?>
                                <?php if ( $segundo_botao ) : ?>
                                    <button class="btn btn_first mb-4"><a class="text-uppercase text-white" href="<?php echo esc_url( $segundo_botao['url'] ); ?>" target="<?php echo esc_attr( $segundo_botao['target'] ); ?>"><?php echo esc_html( $segundo_botao['title'] ); ?></a></button>
                                <?php endif; ?>
                        </div>
                    </div>
                </div>
            </section><!--/.como-funciona-->
		<?php elseif ( get_row_layout() == 'comparativo' ) : ?>
            <section class="comparacao pb-0">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="card col-md-6">
                            <h2 class="pb-3"><?php the_sub_field( 'como_funciona_titulo' ); ?></h2>
                            <p><?php the_sub_field( 'subtitulo' ); ?></p>
                            <?php $baixarbotao = get_sub_field( 'baixarbotao' ); ?>
                                <?php if ( $baixarbotao ) : ?>
                                    <button class="btn btn_first col-md-7 mt-4 mb-4"><a class="text-uppercase text-white" href="<?php echo esc_url( $baixarbotao['url'] ); ?>" target="<?php echo esc_attr( $baixarbotao['target'] ); ?>"><?php echo esc_html( $baixarbotao['title'] ); ?></a></button>
                                <?php endif; ?>
                        </div>
                    </div>
                </div>
            </section><!--/.comparacao-->
		
            <?php elseif ( get_row_layout() == 'produtos_relacionados_catalogos' ) : ?>
                <section class="modelos">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-5">
                                <h2>                <?php the_sub_field( 'conheca_nosso_catalogo_de_produtos_titulo' ); ?></h2>
                                <p><?php the_sub_field( 'confira_todos_os_nossos_produtos_e_encontre_a_solucao_ideal_para_o_seu_projeto_descricao' ); ?></p>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="modelos_carousel">
                                

                                        <?php $produtos_relacionados_relacao_catalogo = get_sub_field( 'produtos_relacionados_relacao_catalogo' ); ?>
                                            <?php if ( $produtos_relacionados_relacao_catalogo ) : ?>
                                                <?php foreach ( $produtos_relacionados_relacao_catalogo as $post ) : ?>
                                                    <?php setup_postdata ( $post ); ?>
                                                    <div class="col-md-3 p-2">
                                                        <div class="card" style="min-height: auto;">
                                                            <div class="card-header">
                                                                <a href="<?php the_permalink(); ?>">
                                                                    <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
                                                                    echo the_post_thumbnail('full');?>
                                                                </a>
                                                            </div>
                                                            <div class="card-content">
                                                                <h3>                                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                   
                                                <?php endforeach; ?>
                                                <?php wp_reset_postdata(); ?>
                                            <?php endif; ?>
                                       


                                        
                                   
                            </div>
                        </div>
                    </div>
                </div>
            </section>
                
     
            
		<?php endif; ?>
	<?php endwhile; ?>
<?php else: ?>
	<?php // no layouts found ?>
<?php endif; ?>


<!--<section class="main-sistema-interno">
    <div class="container">
        <div class="row">
            <div class="col-md-6 box-left">
                <h2>Conheça as fôrmas Planex</h2>
                <p>
                    O Sistema Planex é uma solução completa para obras com laje maciça. 
                    Composto por fôrmas de resina termoplásticas, ele substitui a 
                    utilização de compensados de madeira para concretagem da laje.   
                </p>
            </div>
            <div class="col-md-6">
                <div class="main_sistema_carousel">
                    <div class="card">
                        <div class="card-header">
                            <img src="/wp-content/themes/atex/img/mais.png" />
                            <p>Sustentável</p>
                        </div>
                        <div class="card-content">
                            <p>
                                <b>Reduz até 90% do uso de madeira na região da laje.</b> 
                                Outro fator importante é que ela minimiza a geração 
                                de resíduos na obra. Assim, ela colabora indiretamente 
                                para diminuir a emissão de CO2. No geral, as fôrmas 
                                Planex também contribuem para a certificação LEED ® do 
                                seu projeto. 
                            </p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <img src="/wp-content/themes/atex/img/mais.png" />
                            <p>Sustentável</p>
                        </div>
                        <div class="card-content">
                            <p>
                                <b>Reduz até 90% do uso de madeira na região da laje.</b> 
                                Outro fator importante é que ela minimiza a geração 
                                de resíduos na obra. Assim, ela colabora indiretamente 
                                para diminuir a emissão de CO2. No geral, as fôrmas 
                                Planex também contribuem para a certificação LEED ® do 
                                seu projeto. 
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>/.main-sistema-interno-->

<section class="modelos d-none">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3">
                    <h2>Confira os modelos de Platex disponíveis:</h2>
                </div>
            </div>
            <div class="col-md-12">
                <div class="modelos_carousel">
                    <div class="col-md-3 p-2">
                        <div class="card">
                            <div class="card-header">
                                <img src="/wp-content/themes/atex/img/platexLisa.png" alt="">
                            </div>
                            <div class="card-content">
                                <h3>Platex Lisa</h3>
                                <p>
                                    Fôrma perfeita para garantir um teto uniforme, 
                                    simples e que combina com todos os ambientes. 
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 p-2">
                        <div class="card">
                            <div class="card-header">
                                <img src="/wp-content/themes/atex/img/platexWood.png" alt="">
                            </div>
                            <div class="card-content">
                                <h3>Platex Wood</h3>
                                <p>
                                    O concreto é moldado em uma textura semelhante à madeira. 
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 p-2">
                        <div class="card">
                            <div class="card-header">
                                <img src="/wp-content/themes/atex/img/platexCircle.png" alt="">
                            </div>
                            <div class="card-content">
                                <h3>Platex Circle</h3>
                                <p>
                                    O concreto é moldado com desenhos circulares que possuem 
                                    uma textura especial e um relevo para enfatizar o desenho. 
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 p-2">
                        <div class="card">
                            <div class="card-header">
                                <img src="/wp-content/themes/atex/img/platexFibonacci.png" alt="">
                            </div>
                            <div class="card-content">
                                <h3>Platex Fibonacci</h3>
                                <p>
                                    Desenhos de retângulos sobrepostos proporcionais à 
                                    sequência de Fibonacci marcam o concreto. Como o 
                                    desenho não é simétrico, a montagem das fôrmas Planex 
                                    Fibonacci faz com que cada laje possua um design único. 
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_template_part( 'global/template-part', 'solution' ); ?>

<style>
    .crm-webform-fieldset{
       
        display: flex !important;
        flex-wrap: wrap !important;
    }
    .crm-webform-fieldset .row{
        all: unset
    }
</style>

<?php get_template_part( 'global/template-part', 'tire-duvidas' ); ?>

<?php endwhile; ?>
<?php get_footer(); ?>
