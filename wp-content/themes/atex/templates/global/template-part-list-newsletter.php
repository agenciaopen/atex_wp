<section class="newsletter">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
        <?php the_field( 'titulo_newsletter', 'option' ); ?>
        <?php the_field( 'descricao_newsletter', 'option' ); ?>
        <?php the_field( 'formulario_newsletter', 'option' ); ?>

        </div>
    </div>
</section><!-- /.newsletter -->