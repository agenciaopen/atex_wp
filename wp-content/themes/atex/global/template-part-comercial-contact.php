<section class="comercial-contact">
    <div class="container h-100">
        <div class="row h-100">
            <div class="col-md-4 order-2 order-md-1 mt-4" >
               <img src="\wp-content\themes\atex\img\svg\marker.svg" alt="">
               <h2>Contatos comerciais em</h2>
               <h4 id="titlecc"><strong></strong></h4>
               <p id="telcc"></p>
            </div>
            <div class="col-md-8 order-1 order-md-2">
                <div class="select">
                    <?php echo do_shortcode('[searchandfilter id="316"]')?>
                    <div class="select_arrow">
                    </div>
                </div> 
            </div>
            <div class="col-md-8 offset-md-4 order-3 pl-0">
                <?php echo do_shortcode('[searchandfilter id="316" show="results"]')?>
            </div>
        </div>
    </div>
</section><!-- /.comercial-contact -->

