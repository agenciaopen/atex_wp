<?php
/**
*
* single page for cpt obras
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>
<?php while ( have_posts() ) : the_post(); ?>

    <section class="interna_product">
        <div class="content container">
            <div class="row d-flex justify-content-center">
                <div class="modal-product col-md-12">
                    <div class="close-modal">
                        <a href="/obras">
                            <img src="\wp-content\themes\atex\img\svg\close_modal.svg" alt="">
                        </a>
                    </div>
                    <div class="card col-md-7">
                        <div class="card-header">
                            <h2><?php echo get_the_title();?></h2>
                            <p><?php the_field( 'localidade' ); ?></p>
                        </div>
                        
                        <div class="gallery_obra_carousel">
                            <?php $galeria_de_fotos_images = get_field( 'galeria_de_fotos' ); ?>
                            <?php if ( $galeria_de_fotos_images ) :  ?>
                                <?php foreach ( $galeria_de_fotos_images as $galeria_de_fotos_image ): ?>
                                <div class="card-slider">
                                    <img src="<?php echo esc_url( $galeria_de_fotos_image['sizes']['large'] ); ?>" alt="<?php echo esc_attr( $galeria_de_fotos_image['alt'] ); ?>" />
                                </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                        
                        <div class="card-content">
                            <p class=""><strong>Solução Utilizada: </strong><?php the_field( 'solucao_utilizada' ); ?></p>
                            <p class=""><strong>Metragem: </strong><?php the_field( 'metragem' ); ?></p>
                            <p><strong>Economia: </strong><?php the_field( 'economia' ); ?></p>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <style>
        .footer{
            display:none;
        }
        .comercial-contact{
            display:none;
        }
        #nav_main{
            display:none;
        }
    </style>
<?php endwhile; ?>
<?php get_footer(); ?>
