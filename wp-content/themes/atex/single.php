<?php
get_header();

?>

			<?php
			if ( have_posts() ) :
				while ( have_posts() ) :
					the_post();
                    ?>
                 <?php 
                 
                 if(wp_is_mobile()):
                    $featured_img_url = get_the_post_thumbnail_url(get_the_ID($page_ID),'large'); 
                else:
                    $featured_img_url = get_the_post_thumbnail_url(get_the_ID($page_ID),'large'); 
                endif; 
                ?>
              
                <?php $title = get_the_title(); ?>
                
                <section class="main post" style="background-image: url('/wp-content/uploads/2020/09/banner-home.png');">
                    <div class="container h-100">
                        <div class="row h-100 align-items-center justify-content-center">
                            <div class="col-md-12 text-center banner-text">                
                                <h1 style="font-size: 26px; font-weight: normal; ">
                                    Edifique seu conhecimento sobre
                                </h1>
                                <hr>
                                <h2 style="font-size: 32px;font-weight: bold;">
                                    Edifique seu conhecimento sobre construção civil
                                </h2>
                                <hr>
                                <div class="row m-0 justify-content-center">
                                    <?php echo do_shortcode('[searchandfilter id="1990"]');?>
                                </div>
                            </div>
                                
                        </div>
                    </div>
                </section><!-- /.main -->
                <section class="search_article">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="category_article hide-mobile">
                                    <ul>
                                        <li><a href="/blog">Todos os artigos</a></li>
                                        <?php wp_list_categories(array(
                                            'orderby' => 'name',
                                            'title_li' => '',
                                        )); ?>
                                    </ul>
                                </div>
                                <div class="select hide-desk">
                                    <?php wp_dropdown_categories('show_option_none=Selecione uma categoria'); ?>

                                        <div class="select_arrow">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </section>
                 <section class="blog-post">
                     <div class="container">
                         <div class="row">
                             <div class="col-md-8">
                                <div class="category-post-interna">
                                <?php
                                    the_category();
                                    $categories = get_the_category();
                                    foreach($categories as $key => $category) {
                                        $url = get_term_link((int)$category->term_id,'category');
                                        $categories[$key] =
                                        "<p>{$category->category_description}</p>";
                                    }
                                    //echo "<dl>\n" . implode("\n",$categories) . "\n</dl>";
                                    ?>
                                </div>
                                <h2>
                                    <?php echo $title;?>
                                    <span class="d-block"><?php the_field( 'subtitulop', $page_ID); ?></span>
                                </h2>
                                <div class="date">Publicado por <?php the_author(); ?> em <?php echo get_the_date(); ?></div><br>
                                    <div class="blog-img">
                                        <img src="<?php echo $featured_img_url; ?>" alt="">
                                    </div>
                                <article id="<?php echo $post->ID; ?>" class="section section-text">
                                    <?php the_content();?>
                                </article>
                             </div>
                             <div class="col-md-4">
                                <div class="new-sletters pl-0 pr-0">
                                    <div class="card-header">
                                        <h3><?php the_field( 'titulo_newsletter', 'option' ); ?></h3>
                                        <p><?php the_field( 'descricao_newsletter', 'option' ); ?></p>
                                    </div>    
                                    <div class="card-content">
                                        <?php the_field( 'formulario_newsletter', 'option' ); ?>
                                    </div>
                                </div>
                                <div class="news-post">
                                    <h2>Últimos posts</h2>
                                    <?php 
                                    // Custom WP query query
                                    // Query Arguments
                                    $args_query = array(
                                    'post_status' => array('publish'),
                                    'posts_per_page' => 3,
                                    'post_type' => 'post',
                                    'order' => 'DESC',
                                    );
                                    
                                    // The Query
                                    $query = new WP_Query( $args_query );
                                    
                                    // The Loop
                                    if ( $query->have_posts() ) {
                                    while ( $query->have_posts() ) {
                                    $query->the_post();
                                    // Your custom code ?>
                                        <div class="card pb-4 pt-4">  
                                        <span class="date"><?php echo date("d M", strtotime($query->post_date)); ?></span>
                                            <?php the_category(); ?>
                                                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                                    <h3 class="post-title"><?php if (strlen($post->post_title) > 35) {
                                                        echo substr(the_title($before = '', $after = '', FALSE), 0, 60) . '...'; } else {
                                                        the_title();
                                                        } ?>
                                                    </h3>
                                                </a>
                                        </div>
                                <?php }
                                    } else {
                                    // no posts found
                                    
                                    }
                                    
                                    /* Restore original Post Data */
                                    wp_reset_postdata();
                                    
                                    ?>
                                </div>
                             </div>
                         </div>
                     </div>
                 </section><!-- /.blog-post -->

                 <section class="comentarios">
                    <div class="container h-100">
                        <div class="row h-100 align-items-center justify-content-center">
                            <div class="col-md-12 justify-content-between card">
                                <div class="card-header">
                                    <h3><?php the_field( 'titulo_comentario', 'option' ); ?></h3>
                                    <p><?php the_field( 'descricao_comentario', 'option' ); ?></p>
                                </div>    
                                <div class="card-content">
                                    <?php the_field( 'formulario_comentario', 'option' ); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section><!-- /.newsletter -->
                <section class="post-relacionado">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3 card-right">
                                <?php $page_ID = 2; ?>
                                <h2>Post<br>
                                    Relacionados</h2>
                            </div>
                            <div class="col-md-12">
                                <div class="blog_carousel">
                                    <?php
                                    // Custom WP query query
                                    // Query Arguments
                                    $args_query = wp_get_post_tags($post->ID);
                                    if ($args_query) {
                                        $tag_ids = array();
                                        foreach($args_query as $individual_tag) $tag_ids[] = $individual_tag->term_id;
                                        $args=array(
                                        'tag__in' => $tag_ids,
                                        'post__not_in' => array($post->ID),
                                        'showposts'=>5, // Quantidade de posts relacionados que deverá ser exibida.
                                        'caller_get_posts'=>1
                                        );
                                        $args_query = array(
                                        'post_status' => array('publish'),
                                        'posts_per_page' => 9,
                                        'order' => 'DESC',
                                    );

                                    // The Query
                                    $query = new WP_Query($args_query);

                                    // The Loop
                                    if ($query->have_posts()) {
                                        while ($query->have_posts()) {
                                            $query->the_post();
                                            // Your custom code 
                                    ?>
                                            <div class="card">
                                                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                                    <div class="card-header">
                                                        <?php $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'large'); ?>
                                                        <img src="<?php echo $url ?>" class="img-fluid" />
                                                    </div>
                                                    <div class="card-content">
                                                        <h4 class="date"><b><?php the_date(); ?></b></h4>
                                                        <h4><?php $categories = get_the_category();
                                                            foreach ($categories as $category) {
                                                                //echo $category->name; //category name
                                                                $cat_link = get_category_link($category->cat_ID);
                                                                echo '<strong><a href="' . $cat_link . '">' . $category->name . ' </a></strong> '; // category link
                                                            } ?></h4><br>
                                                        <h3><?php the_title(); ?></h3><br>
                                                        <p><?php 
                                                        $excerpt = get_the_excerpt();
                
                                                        $excerpt = substr($excerpt, 0, 180);
                                                        $result = substr($excerpt, 0, strrpos($excerpt, ' '));
                                                        echo $result;
                                                        ?>...</p> 
                                                        <p class="d-none"><?php echo wp_strip_all_tags(get_the_excerpt(), true); ?></p>
                                                    </div>
                                                </a>
                                            </div>


                                    <?php                            }
                                    } else {
                                        // no posts found

                                    }
                                } else {
                                    // no posts found

                                }

                                    /* Restore original Post Data */
                                    wp_reset_postdata();

                                    ?>

                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <a href="/blog/" target="">
                                    <button class="btn btn_first col-md-3 mt-4 mb-4">Acessar o blog da Atex</button>
                                </a>

                            </div>
                        </div>
                    </div>
                </section>
                 
<?php
				endwhile;
				else :
					get_template_part( 'template-parts/content', 'none' );
			endif;
				?>


	<?php get_footer(); ?>
