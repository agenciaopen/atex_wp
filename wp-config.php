<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'open_atex' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/* Multisite */
define( 'WP_ALLOW_MULTISITE', true );
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'atex.open');
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);


/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '(r.1.#&gHSQ?0X!Zls]E)=2&lJjpJD,#k}?]j?>5l>0v?k|hWDs! rMHf8E+?Ha~' );
define( 'SECURE_AUTH_KEY',  '#>{_(x(bRj43%+V?igB-t=?F(h&!k{hx4:bv_?:VMPb*DD%H:cUVEM9X`BqP#5>d' );
define( 'LOGGED_IN_KEY',    'RQv L$?~(,cRB+KcZh ]A,xUb_|;Y?/(SRe>zezb7Bn|HQQj `5>~~YsSXai0Dz5' );
define( 'NONCE_KEY',        'M`kI/Mx2EmFr=y<4l#OIm&-HLgSBrsb[2T~lEtzn4VG9o`#no93jv28*}p3Af%J4' );
define( 'AUTH_SALT',        'ID/S%3vh :ZI3NCTL6Plqj~VXY_s:qBqGuMn2}Vaoi5DSsu;46hUAzk_8Qi|( ??' );
define( 'SECURE_AUTH_SALT', '[im1FB`h|=IqNie`ehDBQK>a3-4z FDTM;._q|A~3|d2!TW{<s+B~|uSYU4G@Nd<' );
define( 'LOGGED_IN_SALT',   '`,(E.qG>E#K cw>dGB%;K)aI)?Af&G*Qqup6IJ#<+lhQ_L;L*._ej8^kdX T .07' );
define( 'NONCE_SALT',       '^GB<$p+VHS;Ut!|zyk`oBtud}MCzDKl(9YG3w` .mttOcX|ozK[$-$zIBSYBA%NM' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';
