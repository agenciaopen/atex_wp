<?php
/**
*
* Template Name: Interna Product
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>
<?php get_template_part( 'global/template-part', 'banner' ); ?>

<section class="budget">
    <div class="container">
        <div class="row">
            <div class="col-md-12 order-2 order-md-1">
                <h2>Fôrma Unidirecional para laje nervurada</h2>
            </div>
            <div class="col-md-4 order-1 order-md-2 card-img">
                <img src="\wp-content\themes\atex\img\orcamento.png" alt="">
            </div>
            <div class="col-md-6 order-3 card-content">
                <p>
                    Resistentes e flexíveis, as fôrmas unidirecionais da Atex são <b>projetadas para vencer 
                    vãos em que a relação entre o vão menor e o maior, seja inferior à 0,5.</b> Além disso, 
                    também é possível utilizar o anulador de nervuras para transformar as fôrmas bidirecionais 
                    em soluções para construção de lajes nervuradas em uma única direção. 
                </p>
                <p>
                    Quer saber se esse sistema é aplicável ao seu projeto? Solicite uma consultoria gratuita dos nossos especialistas. 
                </p>
            </div>
            <div class="col-md-12 order-4">
                <button class="btn btn_first col-md-3 mt-4 mb-4">Solicitar orçamento</button>
            </div>
        </div>
    </div>
</section>

<section class="product-select">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="select">
                    <select>
                        <option>Atex 600</option>
                        <option>Atex 600</option>
                        <option>Atex 600</option>
                        <option>Atex 600</option>
                        <option>Atex 600</option>
                    </select>
                    <div class="select_arrow">
                    </div>
                </div>
                <div class="product-form">
                    <img src="\wp-content\themes\atex\img\formas_desk.png" alt="" class="hide-mobile">
                    <img src="\wp-content\themes\atex\img\forma.png" alt="" class="hide-desk">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="related">
 <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h2>Produtos <br>relacionados</h2>
            </div>
            <div class="col-md-9">
                <div class="related_carousel">
                    <div class="card">
                        <div class="card-interno">
                            <div class="card-header">
                                <img src="\wp-content\themes\atex\img\aplication.png" alt="">
                            </div>
                            <div class="card-content">
                                <h3>Cabetex</h3>
                                <p>Peça que pode ser acoplada ao escoramento, por réguas complementares, e que permite a desforma no 3º dia após a concretagem.</p>
                                <div class="d-flex justify-content-end">
                                    <button class="btn btn_first mt-4 mb-4">Detalhes do produto</button>
                                </div>
                            </div>
                        </div>  
                    </div>
                    <div class="card">
                        <div class="card-interno">
                            <div class="card-header">
                                <img src="\wp-content\themes\atex\img\aplication.png" alt="">
                            </div>
                            <div class="card-content">
                                <h3>Cabetex</h3>
                                <p>Peça que pode ser acoplada ao escoramento, por réguas complementares, e que permite a desforma no 3º dia após a concretagem.</p>
                                <div class="d-flex justify-content-end">
                                    <button class="btn btn_first mt-4 mb-4">Detalhes do produto</button>
                                </div>
                            </div>
                        </div>  
                    </div>
                                        
                </div>
            </div>
        </div>
    </div>
</section><!-- /.related-->

<section class="possibilities">
 <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h2>Possíveis <br>aplicações</h2>
            </div>
            <div class="col-md-9">
                <div class="possibilities_carousel">
                    <div class="card">
                        <div class="card-header">
                            <img src="\wp-content\themes\atex\img\aplication.png" alt="">
                        </div>
                        <div class="card-content">
                            <h3>Laje nervurada com vigas</h3>
                            <p>As fôrmas para laje nervurada Atex apoiadas em vigas é a solução mais utilizada. Ela oferece facilidade na montagem e reduz até 40% do consumo de concreto e aço na obra.</p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <img src="\wp-content\themes\atex\img\aplication.png" alt="">
                        </div>
                        <div class="card-content">
                            <h3>Laje nervurada com vigas</h3>
                            <p>As fôrmas para laje nervurada Atex apoiadas em vigas é a solução mais utilizada. Ela oferece facilidade na montagem e reduz até 40% do consumo de concreto e aço na obra.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- /.aplication -->

<?php get_template_part( 'global/template-part', 'solution' ); ?>

<?php get_footer(); ?>