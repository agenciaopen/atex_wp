<?php
//  register post type pprodutos 

function open_register_post_type_pprodutos(){

    $labels = array(
        'name'                  => _x( 'Produtos', 'Post type general name', 'open' ),
        'singular_name'         => _x( 'Produto', 'Post type singular name', 'open' ),
        'menu_name'             => _x( 'Produtos', 'Admin Menu text', 'open' ),
        'name_admin_bar'        => _x( 'Produto', 'Adicionar novo on Toolbar', 'open' ),
        'add_new'               => __( 'Adicionar novo', 'open' ),
        'add_new_item'          => __( 'Adicionar novo Produto', 'open' ),
        'new_item'              => __( 'Nova Produto', 'open' ),
        'edit_item'             => __( 'Editar Produto', 'open' ),
        'view_item'             => __( 'Ver Produto', 'open' ),
        'all_items'             => __( 'Todas os Produtos', 'open' ),
        'search_items'          => __( 'Procurar Produtos', 'open' ),
        'parent_item_colon'     => __( 'Parent Produtos:', 'open' ),
        'not_found'             => __( 'Nenhuma Produto foi encontrada', 'open' ),
        'not_found_in_trash'    => __( 'Nenhuma Produto na lixeira', 'open' ),
        'featured_image'        => _x( 'Imagem destacada da Produto', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'open' ),
        'set_featured_image'    => _x( 'Selecionar imagem destacada', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'open' ),
        'remove_featured_image' => _x( 'Remover imagem destacada', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'open' ),
        'use_featured_image'    => _x( 'Usar como imagem destacada', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'open' ),
        'archives'              => _x( 'Arquivos de Produto', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'open' ),
        'insert_into_item'      => _x( 'Inserir no Produto', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'open' ),
        'uploaded_to_this_item' => _x( 'Upload no Produto', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'open' ),
        'filter_items_list'     => _x( 'Filtrar lista de Produtos', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'open' ),
        'items_list_navigation' => _x( 'Produtos list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'open' ),
        'items_list'            => _x( 'Produtos list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'open' ),
    );
 
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'capability_type'    => 'post',
        'rewrite' => array(
          'slug' => 'produtos',
          'with_front' => false
      ),

        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_icon'          => 'dashicons-layout',
        'menu_position'      => null,
        'supports'           => array( 'title', 'author', 'revisions', 'thumbnail', 'editor', 'excerpt' ),
    );
 
    register_post_type( 'pprodutos', $args );

}
add_action ('init', 'open_register_post_type_pprodutos');



  // Let us create Taxonomy for Custom Post Type
  add_action( 'init', 'create_emp_custom_taxonomy_solucoes', 0 );
     
  //create a custom taxonomy name it "type" for your posts
  function create_emp_custom_taxonomy_solucoes() {
   
    $labels = array(
      'name' => _x( 'Soluções', 'taxonomy general name' ),
      'singular_name' => _x( 'Soluções', 'taxonomy singular name' ),
      'search_items' =>  __( 'Procurar por soluções' ),
      'all_items' => __( 'Todos os soluções' ),
      'parent_item' => __( 'Parent soluções' ),
      'parent_item_colon' => __( 'Parent Type:' ),
      'edit_item' => __( 'Editar soluções' ), 
      'update_item' => __( 'Atualizar soluções' ),
      'add_new_item' => __( 'Adicionar novo Soluções' ),
      'new_item_name' => __( 'Novo Soluções' ),
      'menu_name' => __( 'Soluções' ),
    ); 	
   
    register_taxonomy('solucoes',array('pprodutos'), array(
      'hierarchical' => true,
      'labels' => $labels,
      'show_ui' => true,
      'show_admin_column' => true,
      'query_var' => true,
      'rewrite' => array( 'slug' => 'solucoes-produtos' ),
    ));
  }
  
  
    // Let us create Taxonomy for Custom Post Type
    add_action( 'init', 'create_emp_custom_taxonomy_app', 0 );
     
    //create a custom taxonomy name it "type" for your posts
    function create_emp_custom_taxonomy_app() {
     
      $labels = array(
        'name' => _x( 'Aplicações', 'taxonomy general name' ),
        'singular_name' => _x( 'Aplicações', 'taxonomy singular name' ),
        'search_items' =>  __( 'Procurar por Aplicações' ),
        'all_items' => __( 'Todos os Aplicações' ),
        'parent_item' => __( 'Parent Aplicações' ),
        'parent_item_colon' => __( 'Parent Type:' ),
        'edit_item' => __( 'Editar Aplicações' ), 
        'update_item' => __( 'Atualizar Aplicações' ),
        'add_new_item' => __( 'Adicionar novo Aplicações' ),
        'new_item_name' => __( 'Novo Aplicações' ),
        'menu_name' => __( 'Aplicações' ),
      ); 	
     
      register_taxonomy('aplicacoes',array('pprodutos'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'aplicacoes' ),
      ));
    }
    