<?php get_header(''); ?>

<?php
global $post;
$pageID = get_option('page_on_front');

?>
	<?php
		$cat = get_query_var('cat');
		$category = get_category ($cat);
	?>
<?php
$queried_object = get_queried_object();
$taxonomy = $queried_object->taxonomy;
$term_id = $queried_object->term_id;
$term_name = $queried_object->term_name;
$enable_cat =  $taxonomy . '_' . $term_id;

?>

<?php
if(wp_is_mobile()):
				

                    $featured_img_url = get_field( 'imagem_destacada', $enable_cat );; 
                else:
   
					   $featured_img_url = '/wp-content/uploads/2020/09/banner-home.png';;  
                endif;
                ?>
              
                <?php $title = get_the_title(); ?>
                
                <section class="main post" style="background-image: url('<?php echo $featured_img_url;?>');">
                    <div class="container h-100">
                        <div class="row h-100 align-items-center justify-content-center">
                            <div class="col-md-12 text-center">
                                <h1>
									<?php echo $category->name; ?>

                                </h1>
                            </div>
                        </div>
                    </div>
                </section>
				<section class="search_article">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="category_article hide-mobile">

									<ul>
										<li><a href="/blog"><?php echo $category->name; ?></a></li>
										<?php wp_list_categories( array(
											'orderby' => 'name',
											'title_li' => '',
										) ); ?> 
									</ul>
								</div>
								<div class="select hide-desk">
									<?php wp_dropdown_categories( 'show_option_none=Selecione uma categoria' ); ?>

									<div class="select_arrow">
									</div>
								</div>
            				</div>
            				<div class="col-md-12">
								<?php
									$cat = get_query_var('cat');
									$category = get_category ($cat);
								?>
								<?php 
									
									// WP_Query arguments
									$args = array(
										'nopaging'        => false,
										'paged'           => 'paged',
										'posts_per_page'  => '3',
										'category_name'   => $category->slug
									);

									// The Query
									$posts = new WP_Query( $args );
									$cont = 0;

									// The Looppior 
								?>
								<div class="header_article ">
									<h2><?php echo $category->name; ?></h2>
									<p class="d-none">Confira diversos artigos que podem ser alicerces para seus projetos e carreira</p>
									<p><?php echo $category->description; ?></p>
								</div>
								<div class="content_result_article">
								<?php echo do_shortcode('[ajax_load_more container_type="div" scroll="false" post_type="post" posts_per_page="6" button_label="Mostrar mais" button_loading_label="Carregando..." button_done_label="Concluído"]'); ?>
								</div>                        
							</div>
							<?php 
								// Restore original Post Data
								wp_reset_postdata();
								?>
                		</div>
            		</div>
				</section>
				
<?php get_footer(); ?>
