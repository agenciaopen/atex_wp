<?php
/**
*
* Template Name: Trabalhe Conosco
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part( 'global/template-part', 'banner' ); ?>

<section class="trabalhe_conosco">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-md-12">
                <div class="col-md-5 pl-0 pb-4">
                    <h2><?php the_field( 'titulo_' ); ?></h2>
                </div>
            </div>
            <div class="col-md-6 pb-4">
                <p><?php the_field( 'descricao' ); ?></p>
            </div>
            <div class="col-md-6 content-img">
                <?php $imagem = get_field( 'imagem' ); ?>
                <?php if ( $imagem ) : ?>
                    <img class="img-fluid" src="<?php echo esc_url( $imagem['url'] ); ?>" alt="<?php echo esc_attr( $imagem['alt'] ); ?>" />
                <?php endif; ?>
            </div>
        </div>
    </div>
</section><!--/.trabalhe_conosco-->

<section class="form_trabalhe">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3 pl-0 pb-4">
                    <h2><?php the_field( 'titulo_curriculo' ); ?></h2>
                </div>
            </div>
            <div class="col-md-3">
                <?php the_field( 'descricao_curriculo' ); ?>
            </div>
            <div class="col-md-8 form-card">
                <!--<?php// $link_curriculo = get_field( 'link_curriculo' ); ?>
                <?php //if ( $link_curriculo ) : ?>
                    <a href="<?php //echo esc_url( $link_curriculo['url'] ); ?>" target="<?php //echo esc_attr( $link_curriculo['target'] ); ?>"><?php //echo esc_html( $link_curriculo['title'] ); ?></a>
                <?php //endif; ?>-->
                <?php the_field( 'formulario_trabalhe_conosco' ); ?>
            </div>
        </div>
    </div>
</section><!--/.form_trabalhe-->

<?php get_footer(); ?>