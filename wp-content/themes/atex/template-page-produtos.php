<?php
    /**
    *
    * Template Name: Produtos
    *
    */
    
    get_header();
    global $post;
    $page_ID = $post->ID;
    // get page ID
    ?>
<?php get_template_part( 'global/template-part', 'banner' ); ?>
<section id="content" class="d-none">
    <div class="container h-100">
        <div class="row align-items-center justify-content-center h-100">
            <div class="col-md-10 text-center">
                <?php the_field( 'descricao', $page_ID ); ?>
            </div>
        </div>
    </div>
</section>
<!--/.content-->
<section class="product apli">
    <div class="container">
        <div class="row product justify-content-center comercial-contact" style="background: transparent;">
            <div class="col-md-8 card mb-5 py-1 box-filtro">
                <div class="select">
                    <?php echo do_shortcode('[searchandfilter id="1989"]');?>
                    <div class="select_arrow">
                    </div>
                </div> 
            </div>
            <?php echo do_shortcode('[searchandfilter id="1989" show="results"]');?>

            
        </div>
        <?php 
            // Custom WP query query
            // Query Arguments
            $args_query = array(
            'post_status' => array('publish'),
            'posts_per_page' => -1,
            'post_type' => 'pprodutoss',
            'order' => 'DESC',
            );
            
            // The Query
            $query = new WP_Query( $args_query );
            
            // The Loop
            if ( $query->have_posts() ) {
            while ( $query->have_posts() ) {
            $query->the_post();
            // Your custom code ?>
        <div class="row mb-5">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header col-md-3">
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                            <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
                                echo the_post_thumbnail('full');?>
                        </a>
                    </div>
                    <div class="card-description col-md-3">
                        <h3>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                <?php the_title(); ?>
                            </a>
                        </h3>
                        <p>
                            <?php echo wp_strip_all_tags( get_the_excerpt(), true ); ?>
                           
                        </p>
                    </div>
                    <div class="card-soluctions col-md-3">
                        <h3>Usados nas soluções</h3>
                        <ul>
                            <?php
                                $terms = get_terms( 
                                array(
                                'taxonomy' => 'solucoes',
                                
                                ) 
                                );
                                if( $terms):
                                $cont = 1;
                                foreach( $terms as $t ):
                                ?>                
                            <li><img src="./wp-content/themes/atex/img/svg/checked.svg" alt="" class="pr-2"><?php echo $t->name; ?></li>
                            <?php
                                $cont++;
                                endforeach;
                                endif;
                                ?>
                        </ul>
                    </div>
                    <div class="card-related col-md-3">
                        <h3>Produtos Relacionados</h3>
                        <ul>
                            <?php $produtos_relacionados = get_field( 'produtos_relacionados' ); ?>
                            <?php if ( $produtos_relacionados ) : ?>
                                <?php foreach ( $produtos_relacionados as $post ) : ?>
                                    <?php setup_postdata ( $post ); ?>
                                        <li>
                                            <img src="./wp-content/themes/atex/img/svg/checked.svg" alt="" class="pr-2">
                                                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                                    <?php the_title(); ?>
                                                </a>
                                        </li>
                                    <?php endforeach; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <?php                            }
            } else {
            // no posts found
            
            }
            
            /* Restore original Post Data */
            wp_reset_postdata();
            
            ?>
            <!--<div class="col-md-12 d-flex justify-content-center">
                <a href="#" target="#">
                    <button class="btn btn_first mt-4 mx-auto mb-4">Solicite um orçamento</button>
                </a>
            </div>-->
    </div>
</section>
<?php get_template_part( 'global/template-part', 'solution' ); ?>
<?php get_footer(); ?>