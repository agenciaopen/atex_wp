<?php 
add_action('wp_dashboard_setup', 'open_custom_dashboard_widgets');
  
  function open_custom_dashboard_widgets() {
      global $wp_meta_boxes;
      wp_add_dashboard_widget('custom_help_widget', 'Guia de navegação', 'custom_dashboard_help');
  }
   
  function custom_dashboard_help() { ?>
  <style>
          #wpbody-content #dashboard-widgets #postbox-container-1{width:100%}
          .postbox{padding: 15px;}
          .postbox .handlediv{display:none;}
          .meta-box-sortables.ui-sortable .hndle{font-size: 24px; font-weight: normal; border: none; color:#0074a2;}
          .welcome-intro{color:#666; font-size:16px; line-height: 180%;}
          .welcome-box{float:left; width:30%; margin-right:5%; padding: 20px; box-sizing: border-box; border-left: 1px solid #0074a2; margin-top: 20px;}
          .welcome-box-direita{margin-right:0;}
          .welcome-box strong{font-size:20px;font-weight: 400; color: #666;}
          .welcome-box-esquerda strong:before{content: "\f226";font-family: dashicons; color: #666; float: left; margin-right: 5px;}
          .welcome-box-centro strong:before{content: "\f109";font-family: dashicons; color: #666; float: left; margin-right: 5px;}
          .welcome-box-direita strong:before{content: "\f111";font-family: dashicons; color: #666; float: left; margin-right: 5px;}
          
          .quick{margin:40px 0 0;}
          .quick .titulo{font-size:20px; color:#666; font-weight:normal;margin-bottom: 10px; display: inline-block;}
          .quick ul{display: inline-block; vertical-align: top; margin-right: 50px; list-style: initial; margin-left: 20px;}
          .quick a{font-size: 14px; margin-bottom: 8px; display: inline-block; text-decoration: underline !important; font-weight: bold;}
  
          @media(max-width: 768px){
              .welcome-box{float:none; width:100%; margin-right:0; border-top: 1px solid #0074a2; border-left:none;}	
          }
      </style>
  
        <p class="welcome-intro">
            Aqui você pode visualizar todas as páginas existentes em seu site e acessá-las rapidamente. 
        </p> 
      
      <div class="quick">
          <strong class="titulo">Acesso Rápido</strong>
          <br />
          
          <ul>
                <?php 
                $child_pages = new WP_Query( array(
                    'post_type'      => 'page', // set the post type to page
                    'posts_per_page' => -1, // number of posts (pages) to show
                    'no_found_rows'  => true, // no pagination necessary so improve efficiency of loop
                ) );
                if ( $child_pages->have_posts() ) : while ( $child_pages->have_posts() ) : $child_pages->the_post();?>
                    <li>
                        <a href="<?php echo the_permalink(); ?>" target="_blank"><?php echo the_title();?></a>
                    </li>
                <?php endwhile; endif;  
            wp_reset_postdata(); ?>
            </ul>
        </div>
  <?php } ?>
  
  