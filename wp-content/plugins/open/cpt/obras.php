<?php
//  register post type obras 

function open_register_post_type_obras(){

    $labels = array(
        'name'                  => _x( 'Obras', 'Post type general name', 'open' ),
        'singular_name'         => _x( 'Obra', 'Post type singular name', 'open' ),
        'menu_name'             => _x( 'Obras', 'Admin Menu text', 'open' ),
        'name_admin_bar'        => _x( 'Obra', 'Adicionar nova on Toolbar', 'open' ),
        'add_new'               => __( 'Adicionar nova', 'open' ),
        'add_new_item'          => __( 'Adicionar nova obra', 'open' ),
        'new_item'              => __( 'Nova obra', 'open' ),
        'edit_item'             => __( 'Editar obra', 'open' ),
        'view_item'             => __( 'Ver obra', 'open' ),
        'all_items'             => __( 'Todas as obras', 'open' ),
        'search_items'          => __( 'Procurar obras', 'open' ),
        'parent_item_colon'     => __( 'Parent Obras:', 'open' ),
        'not_found'             => __( 'Nenhuma obra foi encontrada', 'open' ),
        'not_found_in_trash'    => __( 'Nenhuma obra na lixeira', 'open' ),
        'featured_image'        => _x( 'Imagem destacada da obra', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'open' ),
        'set_featured_image'    => _x( 'Selecionar imagem destacada', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'open' ),
        'remove_featured_image' => _x( 'Remover imagem destacada', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'open' ),
        'use_featured_image'    => _x( 'Usar como imagem destacada', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'open' ),
        'archives'              => _x( 'Arquivos de obra', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'open' ),
        'insert_into_item'      => _x( 'Inserir na obra', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'open' ),
        'uploaded_to_this_item' => _x( 'Upload na obra', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'open' ),
        'filter_items_list'     => _x( 'Filtrar lista de obras', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'open' ),
        'items_list_navigation' => _x( 'Obras list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'open' ),
        'items_list'            => _x( 'Obras list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'open' ),
    );
 
    $args = array(
        'labels'             => $labels,
        'public'             => false,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite' => array(
          'slug' => 'obras',
          'with_front' => false
      ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_icon'          => 'dashicons-bank',
        'menu_position'      => null,
        'supports'           => array( 'title', 'author', 'revisions', 'thumbnail', 'editor' ),
    );
 
    register_post_type( 'obras', $args );

}
add_action ('init', 'open_register_post_type_obras');


// Let us create Taxonomy for Custom Post Type
add_action( 'init', 'open_create_obras_custom_taxonomy_solution', 0 );
 
//create a custom taxonomy 
function open_create_obras_custom_taxonomy_solution() {
 
    $labels = array(
        'name' => _x( 'Soluções', 'taxonomy general name' ),
        'singular_name' => _x( 'Solução', 'taxonomy singular name' ),
        'search_items' =>  __( 'Procurar soluções' ),
        'all_items' => __( 'Todas as soluções' ),
        'parent_item' => __( 'Parent Solução' ),
        'parent_item_colon' => __( 'Parent Solução:' ),
        'edit_item' => __( 'Editar solução' ), 
        'update_item' => __( 'Atualizar solução' ),
        'add_new_item' => __( 'Adicionar nova solução' ),
        'new_item_name' => __( 'Novo nome para solução' ),
        'menu_name' => __( 'Solução' ),
    ); 	
 
    register_taxonomy('solucao',array('obras'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'solucoes' ),
    ));
}

// Let us create Taxonomy for Custom Post Type
add_action( 'init', 'open_create_obras_custom_taxonomy_type', 0 );
 
//create a custom taxonomy 
function open_create_obras_custom_taxonomy_type() {
 
    $labels = array(
        'name' => _x( 'Tipo de Obra', 'taxonomy general name' ),
        'singular_name' => _x( 'Tipo de obra', 'taxonomy singular name' ),
        'search_items' =>  __( 'Procurar tipos de obras' ),
        'all_items' => __( 'Todas os tipos de obras' ),
        'parent_item' => __( 'Parent Tipos de obra' ),
        'parent_item_colon' => __( 'Parent Tipos de obra:' ),
        'edit_item' => __( 'Editar Tipos de obra' ), 
        'update_item' => __( 'Atualizar Tipos de obra' ),
        'add_new_item' => __( 'Adicionar novo Tipo de obra' ),
        'new_item_name' => __( 'Novo nome para Tipos de obra' ),
        'menu_name' => __( 'Tipos de obra' ),
    ); 	
 
    register_taxonomy('tipo_obra',array('obras'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'tipos-de-obra' ),
    ));
}

  // Let us create Taxonomy for Custom Post Type
  add_action( 'init', 'create_emp_custom_taxonomy_pais2', 0 );
     
  //create a custom taxonomy name it "type" for your posts
  function create_emp_custom_taxonomy_pais2() {
   
    $labels = array(
      'name' => _x( 'País', 'taxonomy general name' ),
      'singular_name' => _x( 'País', 'taxonomy singular name' ),
      'search_items' =>  __( 'Procurar por tipos' ),
      'all_items' => __( 'Todos os paises' ),
      'parent_item' => __( 'Parent tipo' ),
      'parent_item_colon' => __( 'Parent Type:' ),
      'edit_item' => __( 'Editar tipo' ), 
      'update_item' => __( 'Atualizar tipo' ),
      'add_new_item' => __( 'Adicionar novo País' ),
      'new_item_name' => __( 'Novo País' ),
      'menu_name' => __( 'Países' ),
    ); 	
   
    register_taxonomy('paises',array('obras'), array(
      'hierarchical' => true,
      'labels' => $labels,
      'show_ui' => true,
      'show_admin_column' => true,
      'query_var' => true,
      'rewrite' => array( 'slug' => 'paises' ),
    ));
  }

  // Let us create Taxonomy for Custom Post Type
  add_action( 'init', 'create_emp_custom_taxonomy_estado', 0 );
     
  //create a custom taxonomy name it "type" for your posts
  function create_emp_custom_taxonomy_estado() {
   
    $labels = array(
      'name' => _x( 'Estado', 'taxonomy general name' ),
      'singular_name' => _x( 'Estado', 'taxonomy singular name' ),
      'search_items' =>  __( 'Procurar por tipos' ),
      'all_items' => __( 'Todos os tipos' ),
      'parent_item' => __( 'Parent tipo' ),
      'parent_item_colon' => __( 'Parent Type:' ),
      'edit_item' => __( 'Editar tipo' ), 
      'update_item' => __( 'Atualizar tipo' ),
      'add_new_item' => __( 'Adicionar novo Estado' ),
      'new_item_name' => __( 'Novo Estado' ),
      'menu_name' => __( 'Estados' ),
    ); 	
   
    register_taxonomy('estados_obras',array('obras'), array(
      'hierarchical' => true,
      'labels' => $labels,
      'show_ui' => true,
      'show_admin_column' => true,
      'query_var' => true,
      'rewrite' => array( 'slug' => 'estado-obra' ),
    ));
  }
  
    // Let us create Taxonomy for Custom Post Type
    add_action( 'init', 'create_emp_custom_taxonomy_cidade', 0 );
     
    //create a custom taxonomy name it "type" for your posts
    function create_emp_custom_taxonomy_cidade() {
     
      $labels = array(
        'name' => _x( 'Cidade', 'taxonomy general name' ),
        'singular_name' => _x( 'Cidade', 'taxonomy singular name' ),
        'search_items' =>  __( 'Procurar por tipos' ),
        'all_items' => __( 'Todos os tipos' ),
        'parent_item' => __( 'Parent tipo' ),
        'parent_item_colon' => __( 'Parent Type:' ),
        'edit_item' => __( 'Editar tipo' ), 
        'update_item' => __( 'Atualizar tipo' ),
        'add_new_item' => __( 'Adicionar novo Cidade' ),
        'new_item_name' => __( 'Novo Cidade' ),
        'menu_name' => __( 'Cidades' ),
      ); 	
     
      register_taxonomy('cidade',array('obras'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'cidade' ),
      ));
    }
    
    // Let us create Taxonomy for Custom Post Type
    add_action( 'init', 'create_emp_custom_taxonomy_segmento', 0 );
     
    //create a custom taxonomy name it "type" for your posts
    function create_emp_custom_taxonomy_segmento() {
     
      $labels = array(
        'name' => _x( 'Segmento', 'taxonomy general name' ),
        'singular_name' => _x( 'Segmento', 'taxonomy singular name' ),
        'search_items' =>  __( 'Procurar por tipos' ),
        'all_items' => __( 'Todos os tipos' ),
        'parent_item' => __( 'Parent tipo' ),
        'parent_item_colon' => __( 'Parent Type:' ),
        'edit_item' => __( 'Editar tipo' ), 
        'update_item' => __( 'Atualizar tipo' ),
        'add_new_item' => __( 'Adicionar novo Segmento' ),
        'new_item_name' => __( 'Novo segmento' ),
        'menu_name' => __( 'Segmentos' ),
      ); 	
     
      register_taxonomy('segmento',array('obras'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'segmento' ),
      ));
    }
    