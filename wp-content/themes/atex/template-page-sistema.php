<?php
/**
*
* Template Name: Sistema
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part( 'global/template-part', 'banner' ); ?>

<section class="main-sistema">
    <div class="container">
        <div class="row">
            <div class="col-md-6 box-left">
                <h2><?php the_field( 'construa_title' ); ?></h2>
                <p>
                    <?php the_field( 'construa_texto' ); ?>
                </p>
            </div>
            <div class="col-md-6">
                <div class="lajes_carousel">
                <?php if ( have_rows( 'cadastro_de_itens_do_construa' ) ) : ?>
                        <?php while ( have_rows( 'cadastro_de_itens_do_construa' ) ) : the_row(); ?>
                            <div class="card">
                                <div class="card-header">
                                    <?php if ( get_sub_field( 'icone' ) ) : ?>
                                        <img src="<?php the_sub_field( 'icone' ); ?>" />
                                    <?php endif ?>
                                    <p><?php the_sub_field( 'titulo' ); ?></p>
                                </div>
                                <div class="card-content">
                                    <p><?php the_sub_field( 'subtitulo' ); ?></p>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    <?php else : ?>
                        <?php // no rows found ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section><!--/.main-sistema-->

<section class="produtos-solucoes">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h2>Produtos para esta solução</h2>
            </div>
            <div class="col-md-11">
                <div class="p_solucoes_carousel">
                    <div class="card">
                        <div class="card-header">
                            <img src="./wp-content/themes/atex/img/produto.png" alt="">
                        </div>
                        <div class="card-content">
                            <h3>Fôrmas Unidirecional</h3>
                            <p>
                                Resistentes e flexíveis, as fôrmas unidirecionais da 
                                Atex são projetadas para vencer vãos em que a relação 
                                entre o vão menor e o maior seja inferior à 0,5. <b>Seu 
                                uso pode gerar economia de até 40% do uso de concreto 
                                e aço na construção de lajes.</b>
                            </p>
                            <div class="d-flex justify-content-end">
                                <button class="btn btn_first mt-4">Detalhes do produto</button>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <img src="./wp-content/themes/atex/img/produto.png" alt="">
                        </div>
                        <div class="card-content">
                            <h3>Fôrmas Unidirecional</h3>
                            <p>
                                Resistentes e flexíveis, as fôrmas unidirecionais da 
                                Atex são projetadas para vencer vãos em que a relação 
                                entre o vão menor e o maior seja inferior à 0,5. <b>Seu 
                                uso pode gerar economia de até 40% do uso de concreto 
                                e aço na construção de lajes.</b>
                            </p>
                            <div class="d-flex justify-content-end">
                                <button class="btn btn_first mt-4">Detalhes do produto</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!--/.peodutos-solucoes-->

<?php get_template_part( 'global/template-part', 'comparacao' ); ?>

<?php get_template_part( 'global/template-part', 'movie' ); ?>

<?php get_template_part( 'global/template-part', 'solution' ); ?>

<?php get_template_part( 'global/template-part', 'tire-duvidas' ); ?>


<?php get_footer(); ?>