<?php


function open_admin_theme_style() {
    $plugin_url = plugin_dir_url( __FILE__ );

    wp_enqueue_style( 'style',  $plugin_url . "/css/admin_style.css");
}

add_action( 'admin_print_styles', 'open_admin_theme_style' );

//remove wordpress thanks 
add_filter( 'admin_footer_text', '__return_empty_string', 11 );
