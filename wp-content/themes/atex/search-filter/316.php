<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $query->have_posts() )
{
	?>
	
	<!-- Found <?php // echo $query->found_posts; ?> Results<br /> -->
	<ul class="contact-list">
		<?php
			while ($query->have_posts())
			{
				$query->the_post();
				
				?>
				<input id="npublico" class="hidden d-none" value="<?php the_field( 'nome_publico' ); ?>">
				<input id="phone" class="hidden d-none" value="<?php the_field( 'telefone' ); ?>">


				<?php if ( have_rows( 'cadastro_de_comerciais' ) ) : ?>
					<?php while ( have_rows( 'cadastro_de_comerciais' ) ) : the_row(); ?>
						<li class="col-md-6">
							<img src="\wp-content\themes\atex\img\svg\contacts.svg" alt="">
							<div class="dados">
								<p><?php the_sub_field( 'nome' ); ?></p>
								<p><?php the_sub_field( 'telefone' ); ?></p>
								<p><?php the_sub_field( 'e-mail' ); ?></p>
							</div>
						</li>
					<?php endwhile; ?>
				<?php else : ?>
					<?php // no rows found ?>
				<?php endif; ?>
							
			<?php
		}
	?>
    </ul>
<?php
}
else
{
	?>
	<div class='search-filter-results-list text-center mt-4 d-none' data-search-filter-action='infinite-scroll-end'>
		<span>Final dos resultados</span>
	</div>
	<?php
}
?>
<script>
	
	var title = document.getElementById("npublico").value;
	var tel = document.getElementById("phone").value;

	console.log(title);
	console.log(tel);
	tel_href = tel.replace(/\D/g,'');
	if (title ==''){

	}else{
		$('#titlecc').html('<strong>' + title + '</strong>');
		$('#telcc').html('<a href="tel:+' + tel_href + '" id="tel_link">' + tel + '</a>');
	}

</script>