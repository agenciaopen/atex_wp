<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $query->have_posts() )
{
	?>
	<div class="row">
	
	<!-- Found <?php // echo $query->found_posts; ?> Results<br /> -->
		<ul class="list-search-geral">
				<?php
					while ($query->have_posts())
					{
					$query->the_post();
				
					?>
					<?php
					$featured_img_url = get_the_post_thumbnail_url(get_the_ID($post),'full'); 

					if ( $featured_img_url ) :
						$featured_img_url = get_the_post_thumbnail_url(get_the_ID($post),'full'); 
					else :
						$featured_img_url = '/wp-content/uploads/2020/09/5cd46a50e763334d7a4647357cb5f64d.png';

					endif;
					?>
						<li>
							<h3><?php echo the_title(); ?></h3><br>
							

							<p><?php 
							$excerpt = get_the_excerpt();

							$excerpt = substr($excerpt, 0, 240);
							$result = substr($excerpt, 0, strrpos($excerpt, ' '));
							echo $result;
							?>...</p>
							<p class="d-none"><?php echo wp_strip_all_tags(get_the_excerpt(), true); ?></p>
							<a href="<?php the_permalink(); ?>">
								<button class="btn btn_first mt-4 mb-4">Conferir esse item</button>
							</a>
							
						</li>
													
				<?php } ?>
				</div>
			<?php
			}
			else
			{
			?>
	</ul>
	<div class='search-filter-results-list text-center mt-4 d-none' data-search-filter-action='infinite-scroll-end'>
		<span>Final dos resultados</span>
	</div>
	<?php
}
?>

<script>
	
	var title = $("select[name='_sfm_titulo_empreendimento[]'] option:not(.sf-item-0):selected" ).text();
	console.log(title);
	if (title ==''){

	}else{
		$('#title').html('<h2>Empresas parceiros no projeto do <br>' + title + '</h2> <p>Aqui estão listadas todas as empresas que participaram do projeto do <b>' + title + '</b>, desde seu planejamento até sua construção e preservação. Somos gratos a todos os envolvidos nessa parceria. </p>');
	}
</script>