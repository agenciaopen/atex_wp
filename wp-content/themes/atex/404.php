<?php
/**
*
* 404 pages (not found)
*
*/

get_header();
global $post;
$page_ID = 136;
// get page ID
?>

<?php
if(wp_is_mobile()):
                    $featured_img_url = get_the_post_thumbnail_url(get_the_ID($page_ID),'full'); 
                else:
                    $featured_img_url = get_the_post_thumbnail_url(get_the_ID($page_ID),'large'); 
                endif;
                ?>
              
                <?php $title = get_the_title(); ?>
                
                <section class="main post" style="background-image: url('/wp-content/uploads/2020/09/banner-home.png');">
                    <div class="container h-100">
                        <div class="row h-100 align-items-center justify-content-center">
                            <div class="col-md-12 text-center">
                                <h1>Página não encontrada</h1><hr>
                                <p>Não achamos esta página, mas podemos construir uma ponte até o que você procura.</p>
                            </div>
                            <div class="form-404">
                                <?php echo do_shortcode('[searchandfilter id="1998"]');?>
                            </div>
                        </div>
                    </div>
                </section><!-- /.main -->
                <section class="content py-3">
                    <div class="container h-100">
                        <div class="row align-items-center justify-content-center h-100">
                            <div class="col-md-12 text-center">
                                
                            </div>
                            <div class="col-md-12 text-center my-4">
                                
                            </div>
                        </div>
                    </div>
                </section><!--/.content-->
                <?php get_template_part( 'global/template-part', 'solution' ); ?>


<?php get_footer(); ?>




             