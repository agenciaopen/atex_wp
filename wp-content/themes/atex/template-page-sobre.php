<?php

/**
 *
 * Template Name: Institucional
 *
 */

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part('global/template-part', 'banner'); ?>

<section class="history">
    <div class="container h-100">
        <div class="row h-100">
            <div class="col-md-6 history_content">
                <h2><?php the_field('titulo_historia'); ?></h2>
                <?php the_field('descricao_historia'); ?>
            </div>
            <div class="col-md-6">
                <div class="history_carousel">
                    <?php if (have_rows('cadastro_de_itens_da_historia')) : ?>
                        <?php while (have_rows('cadastro_de_itens_da_historia')) : the_row(); ?>
                            <div class="card">
                                
                                <div class="card-header">
                                    <?php if (get_sub_field('icone')) : ?>
                                        <img src="<?php the_sub_field('icone'); ?>" />
                                    <?php endif ?>
                                </div>
                                <div class="card-content">
                                    <strong><?php the_sub_field('titulo'); ?></strong>
                                    <p><?php the_sub_field('subtitulo'); ?></p>
                                </div>
                                <div class="col-md-12 text-right pdb-4">
                                    <div class="card-date text-left">
                                        <?php the_sub_field('ano'); ?>
                                    </div>
                                    <img class="d-none" src="<?php echo get_template_directory_uri(); ?>/img/icon.png" alt="">
                                </div>
                                

                            </div>
                        <?php endwhile; ?>
                    <?php else : ?>
                        <?php // no rows found 
                        ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/.history-->

<section class="obra-about">
    <div class="container">
        <div class="row d-flex justify-content-between">
            <div class="col-md-4 ">
                <h2>Por que escolher Atex para sua obra?</h2>
            </div>

            <div class="col-md-7">
                <ul class="obra_atex">
                    <?php if (have_rows('privilegios', 'option')) : ?>
                        <?php while (have_rows('privilegios', 'option')) : the_row(); ?>
                            <?php if (get_sub_field('privilegios_icons')) : ?>
                                <li class="obra_content">
                                    <img src="<?php the_sub_field('privilegios_icons'); ?>" />
                                <?php endif ?>
                                <!-- alinhar sobre essa verificação -->
                                <p><br><b><?php the_sub_field('privilegios_texto'); ?></b></br></p>
                                </li>
                            <?php endwhile; ?>
                        <?php else : ?>
                            <?php // no rows found 
                            ?>
                        <?php endif; ?>
                </ul>
                <button class="btn btn_first mt-4 mb-4">Conhecer soluções modulares Atex</button>
            </div>
            <!-- alinhar sobre essa id e estrutura de botão -->
        </div>
    </div>
</section><!-- /.obra-about -->

<section class="sustainability">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="content-header">
                    <h2><?php the_field('titulo_sustentabilidade'); ?></h2>
                    <p><?php the_field('descricao_sustentabilidade'); ?></p>
                </div>
            </div>
            <div class="col-md-12">
                <div class="sustainability_carousel">
                    <?php if (have_rows('cadastro_de_itens_sustentabilidade')) : ?>
                        <?php while (have_rows('cadastro_de_itens_sustentabilidade')) : the_row(); ?>
                            <div class="card">
                                <div class="card-header">
                                    <?php if (get_sub_field('icone')) : ?>
                                        <img src="<?php the_sub_field('icone'); ?>" />
                                    <?php endif ?>
                                    <?php the_sub_field('titulo'); ?>
                                </div>
                                <div class="card-content">
                                    <?php the_sub_field('descricao'); ?>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    <?php else : ?>
                        <?php // no rows found 
                        ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/.sustentabilidade-->

<section class="cycle">
    <div class="container">
        <div class="row d-md-none">
            <div class="col-md-12">
                <h2 class="mb-3"><?php the_field('titulo_ciclo'); ?></h2>
                <div class="cycle_carousel">
                    <?php if (have_rows('cadastro_de_itens_do_ciclo')) : ?>
                        <?php while (have_rows('cadastro_de_itens_do_ciclo')) : the_row();
                            $ctd++;
                        ?>
                            <div class="card">
                                <div class="card-header">
                                    <span class="numbers">
                                        <?php echo $ctd; ?>
                                    </span>
                                    <?php if (get_sub_field('icone')) : ?>
                                        <img src="<?php the_sub_field('icone'); ?>" />
                                    <?php endif ?>
                                </div>
                                <div class="card-content">
                                    <?php the_sub_field('descricao'); ?>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    <?php else : ?>
                        <?php // no rows found 
                        ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="row d-none d-md-block cycle_box">
            <div class="col-md-12 text-center">
                <h2><?php the_field('titulo_ciclo'); ?></h2>
                <?php $desktop_ciclo = get_field( 'desktop_ciclo' ); ?>
                <?php if ( $desktop_ciclo ) : ?>
                    <img class="img-fluid mt-3" src="<?php echo esc_url( $desktop_ciclo['url'] ); ?>" alt="<?php echo esc_attr( $desktop_ciclo['alt'] ); ?>" />
                <?php endif; ?>
                <?php $desktop_ciclo2 = get_field( 'ciclo2' ); ?>
                <?php if ( $desktop_ciclo2 ) : ?>
                    <img class="img-fluid mt-3" src="<?php echo esc_url( $desktop_ciclo2['url'] ); ?>" alt="<?php echo esc_attr( $desktop_ciclo2['alt'] ); ?>" />
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<!--/.cycle-->

<section class="quality">
    <div class="container">
        <div class="row d-flex justify-content-between">
            <div class="col-md-5 p-0">
                <h2><?php the_field('titulo_qualidade_reconhecida'); ?></h2>
                <p><?php the_field('descricao_qualidade_reconhecida'); ?></p>
            </div>
            <div class="col-md-6">
                <div class="quality_carousel">
                    <?php if (have_rows('cadastro_de_itens_de_qualidade')) : ?>
                        <?php while (have_rows('cadastro_de_itens_de_qualidade')) : the_row(); ?>
                            <div class="card">
                                <div class="card-header">
                                    <?php if (get_sub_field('imagem')) : ?>
                                        <img src="<?php the_sub_field('imagem'); ?>" />
                                    <?php endif ?>
                                </div>
                                <div class="card-content">
                                    <h3><?php the_sub_field('titulo'); ?></h3>
                                    <p><?php the_sub_field('descricao'); ?></p>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    <?php else : ?>
                        <?php // no rows found 
                        ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/.quality-->

<?php get_template_part('global/template-part', 'avaliacoes'); ?>

<?php get_template_part('global/template-part', 'blog'); ?>

<?php get_footer(); ?>